from itertools import zip_longest
import csv
# open the strange econded sosci file and convert it to usable csv file


gerf = open("translation.ger-eng.csv", mode='r', encoding='utf-16-le', newline='')
engf = open("translation.eng-ger.csv", mode='r', encoding='utf-16-le', newline='')
outf = open("output.ger-eng.csv", mode='w', encoding='utf-16-le')

gerl = csv.reader(gerf, delimiter='\t')
engl = csv.reader(engf, delimiter='\t')

#print(f"Leng Deu : {len(gerl)}  -  Eng : {len(engl)}")

for deu, eng in zip_longest(gerl, engl, fillvalue=None):
    if eng:
        try:
            de1, de2 = deu[0], deu[1]
        except:
            de1 = deu
        try:
            eng1, eng2 = eng[0], eng[1]
        except:
            eng1 = eng
        outf.write(f"{de1}\t{eng1}\n")
    else:
        outf.write("\t".join(deu))

gerf.close()
engf.close()
outf.close()