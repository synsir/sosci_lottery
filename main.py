import csv
import datetime
import random
import json

# define which states are to be checked to determinfe eligibility
checked_states = [ "STATUS 5", "STATUS 6", "STATUS 7", "STATUS 8" ]

# open the utf-16 sosci file and convert it to usable utf-8 csv file
with open("fixed_panel.csv", "w") as of:
    with open("panel.csv", mode='r', encoding='utf-16-le') as f:
        for line in f:
            # replace tabulators with commata
            of.write(line.replace("\t", ","))

# read out all the participants from the fixed file
with open("fixed_panel.csv", "r") as f:
    reader = csv.DictReader(f)
    fields = reader.fieldnames
    participants = list(reader)

today = datetime.date.today()
print(f"\n\n{today}: There are {len(participants)} participants in total.\n")
print(f"Checked States: {checked_states}\n")
COMPLETED_STATE = "3" # this is the code for a completed questionaire

# compile a list of emails that have completed all required states
eligibles = []
for index, part in enumerate(participants):
    # for every participant, assume they are eligible. than check ever checked_state number.
    # if they didn't reach "COMPLETED_STATE" for one of them, don't add them to the eligible list
    eligible = True
    for state in checked_states:
        if part[state] != COMPLETED_STATE:
            eligible = False
            break
    if eligible:
        participant = {
            "index" : index,
            "email" : part['\ufeff\"EMAIL\"']
        }
        eligibles.append(participant)

print(f"{len(eligibles)} participants are eligible\n")
# write the elgiible list of this drawing out in file
with open("eligibles.json", "w") as f:
    json.dump(eligibles, f, indent=4, sort_keys=True)

# finally draw 10 winners using pythons built in sample function
# https://docs.python.org/3/library/random.html#random.sample
winners = random.sample(eligibles, 10)
print("And the winners are:\n")
with open("winners.json", "w") as f:
    json.dump(winners, f, indent=4, sort_keys=True)
for w in winners:
    print("{index}\t{email}".format(**w))
